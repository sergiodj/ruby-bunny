ruby-bunny (2.14.4-3) unstable; urgency=medium

  * Team upload
  * d/ruby-tests.rake: skip a test consistently failing in s390x
  * d/control: use secure url in Homepage field

 -- Lucas Kanashiro <lucas.kanashiro@canonical.com>  Wed, 15 Apr 2020 10:04:15 -0300

ruby-bunny (2.14.4-2) unstable; urgency=medium

  * Team upload
  * debian/ruby-tests.rake: backport to work on ruby2.5
  * debian/ruby-tests.rake: skip tests that consistently fail under ruby2.5

 -- Antonio Terceiro <terceiro@debian.org>  Sat, 28 Mar 2020 06:48:32 -0300

ruby-bunny (2.14.4-1) unstable; urgency=medium

  * Team upload.

  [ Daniel Leidert ]
  * New upstream version.
  * d/compat: Remove obsolete file.
  * d/control: Add Rules-Requires-Root field.
    (Build-Depends): Use debhelper-compat version 12 and add versions for
    ruby-amq-protocol.
    (Standards-Version): Bump to 4.5.0.
    (Depends): Use ${ruby:Depends} and remove interpreter.
    (Homepage): Fix URL (secure URL does not exist).
  * d/upstream/metadata: Add metadata.
  * New upstream version 2.14.4
  * Refresh patch

  [ Antonio Terceiro ]
  * debian/watch: point at github to get full source
  * New upstream version 2.14.4
    - Builds fine (Closes: 952052)
  * debian/ruby-tests.rake: fix starting the RabbitMQ server
  * Add patch to avoid connection to the internet (Closes: #856838)
  * Add patch to avoid using relative paths from spec/ to lib/. This makes the
    test suite pass under autopkgtest
  * Set Testsuite: autopkgtest to run our own custom invocation for
    gem2deb-test-runner

 -- Antonio Terceiro <terceiro@debian.org>  Sat, 21 Mar 2020 12:56:05 -0300

ruby-bunny (2.9.2-2) unstable; urgency=medium

  * Team upload

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Antonio Terceiro ]
  * autopkgtest: replace default Ruby tests with smoke test (Closes: #901552)
  * Bump Standards-Version to 4.4.0; no changes needed otherwise

 -- Antonio Terceiro <terceiro@debian.org>  Wed, 28 Aug 2019 20:11:10 -0300

ruby-bunny (2.9.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 2.9.2
  * Update years of upstream copyright
  * Update Debian packaging copyright
  * Declare compliance with Debian Policy 4.1.4
  * Depends on ruby-amq-protocol >= 2.2.0
  * Bump debhelper compatibility level to 11
  * debian/copyright: use https:// in Format and Source fields
  * Update VCS urls to point to salsa
  * debian/control: use https:// in Homepage field
  * Refresh patch
  * debian/watch: use https:// instead of http://

 -- Lucas Kanashiro <kanashiro@debian.org>  Fri, 27 Apr 2018 14:11:38 -0300

ruby-bunny (2.6.1-2) unstable; urgency=medium

  * debian/ruby-tests.rake: wait for rabbitmq-server to be online before
    starting the tests (Closes: #847741)

 -- Antonio Terceiro <terceiro@debian.org>  Sun, 11 Dec 2016 14:10:15 -0200

ruby-bunny (2.6.1-1) unstable; urgency=medium

  * Team upload

  [ Cédric Boutillier ]
  * Bump debhelper compatibility level to 9
  * Remove version in the gem2deb build-dependency
  * Use https:// in Vcs-* fields

  [ Antonio Terceiro ]
  * New upstream version 2.6.1
  * update packaging with `dh-make-ruby -w`
  * 0001-spec_helper-remove-some-unecessary-or-unavailable-re.patch: remove
    some unecessary or unavailable requirements from the test suite.
  * Run tests during the build. rabbitmq-server is necessary for that.

 -- Antonio Terceiro <terceiro@debian.org>  Sat, 10 Dec 2016 13:01:44 -0200

ruby-bunny (1.1.2-1) unstable; urgency=low

  * Team upload.

  [ Cédric Boutillier ]
  * use canonical URI in Vcs-* fields
  * debian/copyright: use copyright-format/1.0 official URL for Format field

  [ Jonas Genannt ]
  * Imported Upstream version 1.1.2
  * d/control:
    - removed transitional packages (Closes: #735695)
    - bumped standards version to 3.9.5 (no changes needed)
    - added ruby-amq-protocol as depend, 1.9.2-1~
    - wrap-sort
  * d/rules: remove unneeded bin from package
  * install README.md
  * d/ruby-tests.rb: added basic testing
  * d/copyright:
    - updated copyright to format 1.0
    - added myself
    - updated years of copyright

 -- Jonas Genannt <jonas.genannt@capi2name.de>  Fri, 14 Feb 2014 18:14:06 +0100

ruby-bunny (0.8.0-1) unstable; urgency=low

  * Team Upload

  [Cédric Boutillier]
  * New upstream version
  * Build-depend on gem2deb >= 0.3.0~
  * Update the order of affected packages in lintian-overrides

  [Praveen Arimbrathodiyil]
  * Bump Standards-Version: to 3.9.4 (remove DM-Upload-Allowed)

 -- Cédric Boutillier <cedric.boutillier@gmail.com>  Thu, 28 Jun 2012 15:36:54 +0200

ruby-bunny (0.7.8-1) unstable; urgency=low

  * migrated to gem2deb helper (Closes: #655831)
  * close wrongly filed ITPs (Closes: #624612)
  * new upstream release
  * tests are disabled because it needs network

 -- Praveen Arimbrathodiyil <pravi.a@gmail.com>  Sat, 14 Jan 2012 13:06:29 +0530

libbunny-ruby (0.6.2-3) unstable; urgency=low

  * std-ver -> 3.8.4. No changes needed.
  * Switch to Ruby 1.9.1. Closes: #569860.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Sun, 21 Feb 2010 10:18:15 +0100

libbunny-ruby (0.6.2-2) unstable; urgency=low

  * Fix long descriptions

 -- Bryan McLellan <btm@loftninjas.org>  Fri, 18 Dec 2009 10:43:51 -0800

libbunny-ruby (0.6.2-1) unstable; urgency=low

  * New upstream release
  * Upstream replaces amqp specs with MIT licensed copies

 -- Bryan McLellan <btm@loftninjas.org>  Fri, 18 Dec 2009 09:23:25 -0800

libbunny-ruby (0.6.1-1) unstable; urgency=low

  * New upstream release

 -- Bryan McLellan <btm@loftninjas.org>  Thu, 03 Dec 2009 14:48:30 -0800

libbunny-ruby (0.6.0-1) unstable; urgency=low

  * Initial release (Closes: Bug#558979)

 -- Bryan McLellan <btm@loftninjas.org>  Tue, 01 Dec 2009 15:32:02 -0800

